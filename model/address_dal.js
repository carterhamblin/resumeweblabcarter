var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT c.*, FROM address c ' +
        'WHERE c.address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

// FIRST INSERT THE COMPANY
    var query = 'INSERT INTO address (street, zip_code) VALUES (?)';

    var queryData = [params.address];

    connection.query(query, params.company_name, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var company_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var companyAddressData = [];
        if (params.address_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                companyAddressData.push([company_id, params.address_id[i]]);
            }
        }
        else {
            companyAddressData.push([company_id, params.address_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [companyAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var addressInsert = function(address_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO address_address (address_id, street, zip_code) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var addressAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            addressAddressData.push([address_id, addressIdArray[i]]);
        }
    }
    else {
        addressAddressData.push([address_id, addressIdArray]);
    }
    connection.query(query, [addressAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.addressInsert = addressInsert;

//declare the function so it can be used locally
var addressDeleteAll = function(address_id, callback){
    var query = 'DELETE FROM address_address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.addressDeleteAll = addressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE address SET address_name = ? WHERE address_id = ?';
    var queryData = [params.address_name, params.zip_code];

    connection.query(query, queryData, function(err, result) {
        //delete address_address entries for this address
        addressDeleteAll(params.address_id, function(err, result){

            if(params.address_id != null) {
                //insert address_address ids
                addressInsert(params.street, params.zip_code, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS address_getinfo;

 DELIMITER //
 CREATE PROCEDURE address_getinfo (_address_id int)
 BEGIN

 SELECT * FROM address WHERE address_id = _address_id;

 SELECT a.*, s.address_id FROM address a
 LEFT JOIN address_address s on s.address_id = a.address_id AND address_id = _address_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL address_getinfo (4);

 */

exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};