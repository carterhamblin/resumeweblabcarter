var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(skill_id, callback) {
    var query = 'SELECT c.*, FROM skill c ' +
        'WHERE c.skill_id = ?';
    var queryData = [skill_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

// FIRST INSERT THE skill
    var query = 'INSERT INTO skill (skill_name, description) VALUES (?)';

    var queryData = [params.skill_name, params.skill_description];

    connection.query(query, [skillskillData], function(err, result){
        callback(err, result);
    });
};



exports.delete = function(skill_id, callback) {
    var query = 'DELETE FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE skill SET skill_name = ? WHERE skill_id = ?';
    var queryData = [params.skill_name, params.description];

    connection.query(query, queryData, function(err, result) {
                callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS skill_getinfo;

 DELIMITER //
 CREATE PROCEDURE skill_getinfo (_skill_id int)
 BEGIN

 SELECT * FROM skill WHERE skill_id = _skill_id;

 SELECT a.*, s.skill_id FROM skill a
 LEFT JOIN skill_skill s on s.skill_id = a.skill_id AND skill_id = _skill_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL skill_getinfo (4);

 */

exports.edit = function(skill_id, callback) {
    var query = 'SELECT * FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};